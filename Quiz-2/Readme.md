# Pilihan Ganda

**1. ReactJS adalah**
- javascript library yang digunakan untuk membangun backend server
- framework javascript yang digunakan untuk membangun user interface
- javascript library yang digunakan untuk membangun user interface **(Jawaban yang benar)**
- framework javascript untuk membangun aplikasi

------------

**2. jika anda mempunyai variabel let name = "John". bagaimana cara yang tepat untuk menampilkannya di dalam JSX**
- `<h1>name</h1>`
- `<h1>{name}</h1>` **(Jawaban yang benar)**
- `<h1>${name}</>`
- `<h1>[name]</>`

------------

**3. dibawah ini penulisan manakah penulisan class yang benar kecuali**
- class Buah {}
- class BuahBuahan {}
- class buahBuahan {} **(Jawaban yang benar)**
- class BuahSegar {}

------------

**4. Apa yang digunakan untuk meneruskan data atau inisialisasi data ke Component dari luar?**
- .data
- .attribute
- .props **(Jawaban yang benar)**
- .arguments

------------

**5. di bawah ini yang termasuk fitur di dalam es6 yaitu**
- var
- function
- let+const **(Jawaban yang benar)**
- object

------------

**6. salah satu perbedaan arrow function dibandingkan dengan function adalah**
- ada variable nya
- ada return nya
- tidak menggunakan this **(Jawaban yang benar)**
- tidak menggunakan return

------------

**7. Bagaimana cara penulisan arrow function?**
- const myFunction = [] => {}
- const myFunction = () => {} **(Jawaban yang benar)**
- const myFunction = ()=> []
- const myFunction = {} => ()

------------

**8. function yang dipanggil ketika function lain selesai menjalankan programnya adalah**
- arrow function
- class
- promise
- callback **(Jawaban yang benar)**

------------

**9. Synchronous dan Asynchronous. Synchronous artinya program berjalan secara berurutan sedangkan Asynchronous artinya**
- program yang sinkron beraturan
- program yang tidak berjalan
- program yang berjalan secara bersama-sama **(Jawaban yang benar)**
- program yang dimatikan

------------

**10. berikut ini hal-hal yang penting di dalam ReactJS kecuali**
- components
- virtual DOM
- JSX
- CSV **(Jawaban yang benar)**

------------

# Essay

**11. gunakan promise di bawah ini lalu jalankan lah promise dengan memasukkan filter cek colorful atau tidak (nilainya bisa true atau false) dan minimum jumlah halaman (nilainya bisa lebih dari 0). perhatian masukkan promise di bawah ini dalam jawaban juga, dan tampilkan datanya dengan console.log**

![instruksi11.png](instruksi11.png?raw=true)

**Jawaban:**

```javascript
function filterBooksPromise(colorful, amountOfPage){
  return new Promise(function(resolve, reject){
    var books=[
        {name: "shinchan", totalPage: 50, isColorful: true},
        {name: "Kalkulus", totalPage: 250, isColorful: false},
        {name: "doraemon", totalPage: 40, isColorful: true},
        {name: "algoritma", totalPage: 300, isColorful: false},
    ]
    if (amountOfPage > 0) {
        resolve(books.filter(x=> x.totalPage >= amountOfPage && x.isColorful == colorful));
      } else {
        var reason= new Error("Maaf parameter salah")
        reject(reason);
      }
  });
}

filterBooksPromise(false, 50)
  .then(function (books) {
    console.log(books)
  })
  .catch(function (error) {
    console.log(error)
  })
```

------------

**12. ikutilah instruksi di bawah ini**
keterangan: class Lingkaran dan Persegi itu inheritance ke class BangunDatar

![instruksi12.png](instruksi12.png?raw=true)

**Jawaban:**

```javascript
class BangunDatar {
  constructor(nama){
    this.nama = nama
  }

  luas(){
    return ""
  }

  keliling(){
    return ""
  }
}

class Lingkaran extends BangunDatar{

  constructor(nama){
    super(nama)
    this._jariJari = 0
  }


  get jariJari() {
    return this._jariJari
  }

  set jariJari(angka) {
    this._jariJari = angka
  }

  luas(){
    const pi = this.jariJari % 7 === 0 ? 22/7 : 3.14    
    return pi*this.jariJari*this.jariJari
  }
  
  keliling(){
    const pi = this.jariJari % 7 === 0 ? 22/7 : 3.14
    return 2*pi*this.jariJari
  }

}

class Persegi extends BangunDatar{

  constructor(nama){
    super(nama)
    this._sisi = 0
  }


  get sisi() {
    return this._sisi
  }

  set sisi(angka) {
    this._sisi = angka
  }

  luas(){
    return this.sisi*this.sisi
  }
  
  keliling(){
    return 4*this.sisi
  }

}
var bangunDatar1 = new BangunDatar("bangunDatar1")
var lingkaran1 = new Lingkaran("lingkaran1")
lingkaran1.jariJari = 7

var persegi1 = new Persegi("persegi1")
persegi1.sisi = 8
console.log("luas: " + bangunDatar1.luas())
console.log("keliling: " + bangunDatar1.keliling())
console.log()

console.log("luas Lingkaran dan keliling Lingkaran dengan jari-jari : " + lingkaran1.jariJari )
console.log("luas: " + lingkaran1.luas())
console.log("keliling: " + lingkaran1.keliling())
console.log()

console.log("luas Lingkaran dan keliling Lingkaran dengan sisi : " + persegi1.sisi )
console.log("luas: " + persegi1.luas())
console.log("keliling: " + persegi1.keliling())
```

------------

**13. const data = [{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }]**

![instruksi13.png](instruksi13.png?raw=true)

**jawaban**

```javascript
// replace App.js with this code

import React, {Component} from 'react';

class UserInfo extends Component{
  render(){
    const img={
      width: "100%",
      "object-fit": "cover",
      height: "200px",
      "border-top-left-radius": "10px",
      "border-top-right-radius": "10px"
    }
    const card = {
      border: "1px solid black",
      display: "inline-block",  
      width: "40%",
      margin: "10px",
      "border-radius": "10px",
      "box-shadow": "#aaa"
    }

    const container={
      padding: "10px"
    }

    var callname = this.props.item.gender === "Male" ? "Mr" : "Mrs"
    return (
      <div style={card}>
        <img src={this.props.item.photo} style={img} alt="Avatar"/>
        <div style={container}>
          <h4><b>{callname} {this.props.item.name}</b></h4> 
          <p>{this.props.item.profession}</p> 
          <p>{this.props.item.age} years old</p> 
        </div>
      </div>
    ) 
  }
}


function App() {
  const data = [
    {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"},
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"},
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"},
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
  ]
  return (
    <>
      <div style={{width: "60%", margin: "0 auto"}}>
        {
          data.map((el, idx)=>{
            return(          
              <UserInfo key={idx} item={el}/>
            )
          })
        }
      </div>

    </>
  );
}

export default App;

```

------------

**14. buatlah arrow function volume balok dan kubus, gunakan rest parameter di parameter functionnya. lalu tampilkan hasil perhitungannya dengan template literal**

**Jawaban**

```javascript
const volumeBalok = (...rest) => {
    let [panjang, lebar, tinggi] = rest
    let volume = panjang*lebar*tinggi
    return `hasil volume balok dari panjang ${panjang}, lebar ${lebar} dan tinggi ${tinggi} adalah ${volume}`
} 


const volumeKubus = (...rest) => {
    let sisi = rest
    let volume = sisi*sisi*sisi
    return `hasil volume kubus dari sisi ${sisi} adalah ${volume}`
}

console.log(volumeBalok(5,6,7))
console.log(volumeKubus(8))

```

**15. ikutilah instruksi di bawah ini, lalu tampilkan hasilnya dengan console.log**

![instruksi15.png](instruksi15.png?raw=true)

**Jawaban:**

```javascript
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

let combinedBuku = {...buku, 
  warnaSampul: [...buku.warnaSampul, ...warna],
  ...dataBukuTambahan
}

console.log(combinedBuku)

```